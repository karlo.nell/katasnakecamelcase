using KataSnakeCamelCase;
using System;
using Xunit;


namespace KataSnakeCamelCaseTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("hello_edabit", "helloEdabit")]
        [InlineData("is_modal_open", "isModalOpen")]
        public void ToCamelCase(string input, string expected)
        {
            //Arrange
            string actual;
            //Act
            actual = Program.ToCamelCase(input);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData("helloEdabit", "hello_edabit")]
        [InlineData("getColor", "get_color")]
        [InlineData("getColorCode", "get_color_code")]
        public void ToSnakeCase(string input, string expected)
        {
            //Arrange
            string actual;
            //Act
            actual = Program.ToSnakeCase(input);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
